/*
 * File:   main.c
 * Author: eluinstra
 *
 * Created on July 27, 2016, 2:36 PM
 */

#include "mcc_generated_files/mcc.h"
#include "main.h"

bool listening = false;
#if MAX_SAMPLE_DURATION == 0
uint16_t samples[MAX_SAMPLE_SIZE];
#else
uint16_t tmr1_high = 0;
uint32_t samples[MAX_SAMPLE_SIZE];
#endif
uint16_t sample_nr = 0;

/*
												 Main application
 */
void main(void)
{
	// initialize the device
	SYSTEM_Initialize();

	// Enable the Global Interrupts
	INTERRUPT_GlobalInterruptEnable();

	// Enable the Peripheral Interrupts
	INTERRUPT_PeripheralInterruptEnable();

	printf("\033[2J\033[0;0H");
	printf("Microchip IR Scanner\r\n");

	while (1)
	{
		// Add your application code
	}
}

void print_samples()
{
	printf("IR signal detected\r\n");
	int i = 0;
	while (i < sample_nr)
#if MAX_SAMPLE_DURATION == 0
		printf("%u\t%u\r\n",!(i & 1),samples[i++]);
#else
		printf("%u\t%lu\r\n",!(i & 1),samples[i++]);
#endif
}

void handleIR()
{
	IR_INPUT_GetValue();
	if (/*(IR_INPUT_GetValue() != (sample & 1)) && */listening)
	{
#if MAX_SAMPLE_DURATION == 0
		samples[sample_nr++] = TMR1_ReadTimer();
#else
		samples[sample_nr++] = ((uint32_t)tmr1_high << 16) | TMR1_ReadTimer();
#endif
		TMR1_Reload();
#if MAX_SAMPLE_DURATION > 0
		tmr1_high = 0;
#endif
		if (sample_nr == MAX_SAMPLE_SIZE)
		{
			listening = false;
			print_samples();
			__delay_ms(1000);
			sample_nr = 0;
		}
	}
	else if (sample_nr == 0 && IR_INPUT_GetValue() == LOW)
	{
		TMR1_Reload();
#if MAX_SAMPLE_DURATION > 0
		tmr1_high = 0;
#endif
		listening = true;
	}
}

void handleTMR1()
{
#if MAX_SAMPLE_DURATION == 0
	if (listening)
#else
	tmr1_high++;
	if (listening && tmr1_high >= MAX_SAMPLE_DURATION)
#endif
	{
		listening = false;
		print_samples();
		__delay_ms(1000);
		sample_nr = 0;
	}
}

/**
 End of File
 */