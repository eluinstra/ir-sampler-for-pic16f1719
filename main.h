/* 
 * File:   main.h
 * Author: eluinstra
 *
 * Created on July 27, 2016, 2:36 PM
 */

#ifndef MAIN_H
#define	MAIN_H

#ifdef	__cplusplus
extern "C"
{
#endif

#define MAX_SAMPLE_SIZE 256
#define MAX_SAMPLE_DURATION 3 //+1 times 65365us

void handleIR();
void handleTMR1();

#ifdef	__cplusplus
}
#endif

#endif	/* MAIN_H */

